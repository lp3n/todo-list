import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/controllers/task.controller.dart';

import 'views/app.dart';

void main() => runApp(App());
