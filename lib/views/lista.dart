// ignore_for_file: use_key_in_widget_constructors
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/controllers/task.controller.dart';

import './item.dart';
import '../models/task.dart';

class ListaPage extends StatefulWidget {
  @override
  State<ListaPage> createState() => _ListaPageState();
}

class _ListaPageState extends State<ListaPage> {
  @override
  void initState() {
    super.initState();
    final controller = Provider.of<TaskController>(context, listen: false);
    controller.getTasks();
  }

  @override
  Widget build(BuildContext context) {
    final controller = Provider.of<TaskController>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todo List'),
        centerTitle: false,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed('/create'),
        child: const Icon(Icons.add),
      ),
      body: controller.loading
          ? const CircularProgressIndicator()
          : ListView(
              children: controller.tasks
                  .map((t) => Item(t.texto!, t.concluida!))
                  .toList(),
            ),

      // FutureBuilder<List<Task>>(
      //   future: controller.tasks,
      //   builder: (_, snapshot) {
      //     if (!snapshot.hasData) {
      //       return const CircularProgressIndicator();
      //     }

      //     if (snapshot.hasError) {
      //       return Text(snapshot.error!.toString());
      //     }

      //     return ListView(
      //       children: snapshot.data!
      //           .map((t) => Item(t.texto!, t.concluida!))
      //           .toList(),
      //     );
      //   },
      // ),
    );
  }
}
