import 'package:flutter/material.dart';

class Item extends StatefulWidget {

  // atributo  (variável)
  String texto;
  bool concluida;

  // construtor:
  Item(this.texto, this.concluida);

  @override
  State<Item> createState() => _ItemState();
}

class _ItemState extends State<Item> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
      child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(this.widget.texto),
                Checkbox(
                  value: widget.concluida, 
                  onChanged: (newValue) {
                    widget.concluida = newValue!;
                    setState(() {});
                  },
                ),
              ],
            ),
    );
  }
}