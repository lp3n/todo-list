import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './create.dart';
import 'lista.dart';

import '../controllers/task.controller.dart';

class App extends StatelessWidget {

  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => TaskController(),
      child: MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue
        ),
        home: ListaPage(),
        routes: {
          '/list': (context) => ListaPage(),
          '/create': (context) => CreatePage(),
        }
      ),
    );
  }
}

