// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_literals_to_create_immutables

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:todo_list/controllers/task.controller.dart';
import 'package:todo_list/models/task.dart';

class CreatePage extends StatelessWidget {

  final txtCtrl = TextEditingController();

  save(BuildContext context) {
    final controller = Provider.of<TaskController>(context, listen: false);
    var t = Task(txtCtrl.text, false);
    controller.salvar(t);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nova Tarefa"),
      ),
      body: Column(
        children: [
          TextField(
            controller: txtCtrl,
            obscureText: false,
            keyboardType: TextInputType.text,
            maxLines: 3,
            minLines: 1,
            decoration: InputDecoration(
              hintText: "Tarefa",
              labelText: "Tarefa",
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 20,
            child: ElevatedButton(
              onPressed: () => save(context),
              child: Text('Salvar'),
            ),
          ),
        ],
      ),
    );
  }
}