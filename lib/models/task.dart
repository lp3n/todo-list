class Task {
  // atributos:
  String? id;
  String? texto;
  bool? concluida;

  // construtor:
  Task(this.texto, this.concluida);

  Task.fromJson(Map<String, dynamic> json) {
      id = json['id'];
      texto =  json['texto'];
      concluida =  json['concluida'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'texto': texto,
      'concluida': concluida
    };
  }
}