import 'package:flutter/foundation.dart';

import '../models/task.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

class TaskController extends ChangeNotifier 
{
  final String endpoint = 'https://633ebd8583f50e9ba3b72234.mockapi.io/tasks';

  List<Task> tasks = [];
  bool loading = false;

  getTasks() async {
    loading = true;
    final response = await http.get(Uri.parse(endpoint));
    loading = false;
    if(response.statusCode == 200) {
      Iterable json = jsonDecode(response.body);
      tasks = List<Task>.from(json.map((task)=> Task.fromJson(task)));
      notifyListeners();
    } 
    else {
      throw Exception('Failed to load tasks.');
    }
  }

  Future<void> salvar(Task task) async {

    final payload = jsonEncode(task.toJson());

    final response = await http.post(
      Uri.parse(endpoint),
      body: payload, 
      headers: {
        "Content-Type":"application/json"
      });

    if(response.statusCode == 201) {
      final task = Task.fromJson(jsonDecode(response.body));
      tasks.add(task);
      notifyListeners();
    }
  }
}